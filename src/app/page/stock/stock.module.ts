import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockRoutingModule } from './stock-routing.module';
import { StockInformationComponent } from './stock-information/stock-information.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [
    StockInformationComponent
  ],
  imports: [
    CommonModule,
    StockRoutingModule,
    AutocompleteLibModule
  ]
})
export class StockModule { }
